//
//  CalculatorApp.swift
//  Calculator
//
//  Created by Viswaprasad on 17/04/21.
//

import SwiftUI

@main
struct CalculatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
