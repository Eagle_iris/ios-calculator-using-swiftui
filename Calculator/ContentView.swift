//
//  ContentView.swift
//  Calculator
//
//  Created by Viswaprasad on 17/04/21.
//

import SwiftUI

enum Calculator_Button: String
{
    case one = "1";
    case two = "2";
    case three = "3";
    case add = "+"
    case four = "4";
    case five = "5";
    case six = "6";
    case subtract = "-"
    case seven = "7";
    case eight = "8";
    case nine = "9";
    case divide = "÷"
    case zero = "0";
    case mutliply = "x";
    case equal = "=";
    case clear = "AC"
    case decimal = ".";
    case percent = "%";
    case negative = "-/+";

    
    
    
    var buttonColor: Color
    {
        switch self
        {
        case .add, .subtract, .mutliply, .divide, .equal:
            return .orange
        case .clear, .negative, .percent:
            return Color.gray
        default:
            return Color(.darkGray)
        }
    }
    
    
}

enum Operation {
    case add, subtract, multiply, divide, none
}

struct ContentView: View {

    @State var initialval = "0"
    @State var first_number = 0
    @State var Ongoing_Operation: Operation = .none

    let buttons: [[Calculator_Button]] = [[.clear, .negative, .percent, .divide],[.seven, .eight, .nine, .mutliply],[.four, .five, .six, .subtract],[.one, .two, .three, .add],[.zero, .decimal, .equal],]

    var body: some View
    {
        ZStack
        {
            Color.black.edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            
            VStack
            {
                Spacer()
                
                
                HStack()
                {
                    Text("iOS Calculator App").foregroundColor(.white)
                        
                }
                
                
                
                HStack
                {
                    Spacer()
                    Text(initialval)
                        .bold()
                        .font(.system(size: 80))
                        .foregroundColor(.white)
                }
                .padding()
                
                
                
                
                ForEach(buttons, id: \.self)
                { row in
                    HStack
                    {
                        ForEach(row, id: \.self)
                        { item in
                            Button(action:
                            {
                                self.ifbuttonpressed(button: item)
                            }, label:
                                {
                                Text(item.rawValue)
                                    .font(.system(size: 32))
                                    .frame(
                                        width: self.buttonWidth(item: item),
                                        height: self.buttonHeight()
                                    )
                                    .background(item.buttonColor)
                                    .foregroundColor(.white)
                                    .cornerRadius(self.buttonWidth(item: item)/2)
                               })
                        }
                    }
                    .padding()
                }
            }
        }
    }

    
    
    
    func ifbuttonpressed(button: Calculator_Button)
    {
        switch button
        {
        case .add, .subtract, .mutliply, .divide, .equal:
            
            if button == .add
            {
                self.Ongoing_Operation = .add
                self.first_number = Int(self.initialval) ?? 0
            }
            else if button == .subtract
            {
                self.Ongoing_Operation = .subtract
                self.first_number = Int(self.initialval) ?? 0
            }
            else if button == .mutliply
            {
                self.Ongoing_Operation = .multiply
                self.first_number = Int(self.initialval) ?? 0
            }
            else if button == .divide
            {
                self.Ongoing_Operation = .divide
                self.first_number = Int(self.initialval) ?? 0
            }
            else if button == .equal
            {
                let first_val = self.first_number
                let sec_val = Int(self.initialval) ?? 0
                switch self.Ongoing_Operation
                {
                case .add: self.initialval = "\(first_val + sec_val)"
                case .subtract: self.initialval = "\(first_val - sec_val)"
                case .multiply: self.initialval = "\(first_val * sec_val)"
                case .divide: self.initialval = "\(first_val / sec_val)"
                case .none:
                    break
                }
            }

            if button != .equal
            {
                self.initialval = "0"
            }
        case .clear:
            self.initialval = "0"
        case .decimal, .negative, .percent:
            break
        default:
            let number = button.rawValue
            
            if self.initialval == "0"
            {
                initialval = number
            }
            
            else
            {
                self.initialval = "\(self.initialval)\(number)"
            }
            
        }
    }

    func buttonWidth(item: Calculator_Button) -> CGFloat
    {
        if item == .zero
        {
            return ((UIScreen.main.bounds.width - (5*12)) / 4) * 2
        }
        return (UIScreen.main.bounds.width - (5*12)) / 4
    }

    func buttonHeight() -> CGFloat
    {
        return (UIScreen.main.bounds.width - (5*12)) / 4
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
